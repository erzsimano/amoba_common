package hu.bme.szarch.amoba.common.network;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import hu.bme.szarch.amoba.common.config.Config;

public class NetworkTest {

	MessageHandler messageHandler = new MessageHandler() {

		@Override
		public boolean handleMessage(NetworkMessage message) {
			if (message.getId().equals(getHandleId()))
				return false;
			System.out.println(message.getId());
			return true;
		}

		@Override
		public String getHandleId() {
			return "TEST";
		}
	};

	MessageReceiverThread ort = new MessageReceiverThread(Config.SERVER_PORT);

	@Test
	public void test() {
		NetworkMessageRouter nmr = new NetworkMessageRouter() {
			private Map<String, MessageHandler> map = new HashMap<>();

			@Override
			public void routeMessage(NetworkMessage message) {
				map.get(message.getId()).handleMessage(message);
			}

			@Override
			public void registerMessageHandler(MessageHandler handler) {
				map.put(handler.getHandleId(), handler);
			}
		};

		nmr.registerMessageHandler(messageHandler);
		ort.addMessageRouter(nmr);

		ort.start();

		NetworkMessage object = new MapNetworkObject("TEST");

		MessageSenderThread ost = new MessageSenderThread(object, "localhost", Config.SERVER_PORT);

		ost.start();

		try {
			ort.join();
			ort.stopListening();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
