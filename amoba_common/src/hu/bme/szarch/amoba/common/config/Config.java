package hu.bme.szarch.amoba.common.config;

public class Config {

	public static boolean LOGGING_ENABLED = true;

	public static final String SERVER_ADDRESS = "localhost";

	public static final int SERVER_PORT = 11010;

	public static final int CLIENT_PORT = 6363;

	public static final int TIMEOUT_MILLIS = 500;

	public static final int GAME_PLAYER_IDX_O = 0;

	public static final int GAME_PLAYER_IDX_X = 1;

	public static final String DATA_SENDER_ADDRESS = "sender_address";

	// MESSAGE IDS
	public static final String ID_MARK_DATA = "ID_MARK_DATA";
	public static final String ID_PLAYER_DATA = "ID_PLAYER_DATA";
	public static final String ID_PLAYER_STATS_DATA = "ID_PLAYER_STATS_DATA";
	public static final String ID_CREATE_GAME = "ID_CREATE_GAME";
	public static final String ID_JOIN_GAME = "ID_JOIN_GAME";
	public static final String ID_CONNECTION_REQUEST = "ID_CONNECTION_REQUEST";
	public static final String ID_LOGIN_REQUEST = "ID_LOGIN_REQUEST";
	public static final String ID_LIST_GAMES = "ID_LIST_GAMES";
	public static final String ID_LOGOUT_REQUEST = "ID_LOGOUT_REQUEST";
	public static final String ID_START_GAME = "ID_START_GAME";
	public static final String ID_END_GAME = "ID_END_GAME";
	////////////////

	public static class MarkFields {
		public static final String DATA_GAME_ID = "DATA_GAME_ID";
		public static final String DATA_MARK_COORD_X = "DATA_MARK_COORD_X";
		public static final String DATA_MARK_COORD_Y = "DATA_MARK_COORD_Y";
		public static final String DATA_MARK = "DATA_MARK";
	}

	public static class PlayerFields {
		public static final String DATA_PLAYER_NAME = "DATA_PLAYER_NAME";
		public static final String DATA_PLAYER_WINS = "DATA_PLAYER_WINS";
		public static final String DATA_PLAYER_LOSES = "DATA_PLAYER_LOSES";
	}

	public static class HostGameFields {
		public static final String DATA_CREATOR_USERNAME = "DATA_CREATOR_USERNAME";
		public static final String DATA_CREATE_GAME_WIDTH = "DATA_CREATE_GAME_WIDTH";
		public static final String DATA_CREATE_GAME_HEIGHT = "DATA_CREATE_GAME_HEIGHT";
		public static final String DATA_CREATE_GAME_WIN_CRITERIA = "DATA_CREATE_GAME_WIN_CRITERIA";
	}

	public static class JoinGameFields {
		public static final String DATA_JOIN_USERNAME = "DATA_JOIN_USERNAME";
		public static final String DATA_JOIN_GAME_ID = "DATA_JOIN_GAME_ID";
	}

	public static class LoginFields {
		public static final String DATA_USERNAME = "DATA_USERNAME";
		public static final String DATA_PASSWORD = "DATA_PASSWORD";
	}

	public static class ListGameFields {
		public static final String DATA_GAME_LIST = "DATA_GAME_LIST";
	}

	// RESPONSE DATA KEYS
	public static final String ID_CONNECTION_RESPONSE = "ID_CONNECTION_RESPONSE";
	public static final String DATA_CONNECTION_RESPONSE_MESSAGE = "DATA_CONNECTION_RESPONSE_MESSAGE";
	//////////////////

	// POSSIBLE RESPONSE MESSAGES
	public static final String RESPONSE_MESSAGE_OK = "RESPONSE_MESSAGE_OK";
	public static final String ID_CONNECTION_OK = "ID_CONNECTION_OK";
	public static final String RESPONSE_MESSAGE_ERROR = "RESPONSE_MESSAGE_ERROR";
	public static final String RESPONSE_MESSAGE_INVALID_CREDENTIALS = "RESPONSE_MESSAGE_INVALID_CREDENTIALS";
	public static final String RESPONSE_MESSAGE_GAME_DOES_NOT_EXIST = "RESPONSE_MESSAGE_GAME_DOES_NOT_EXIST";
	//////////////////
}
