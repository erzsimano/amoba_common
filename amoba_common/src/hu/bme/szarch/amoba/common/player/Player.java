package hu.bme.szarch.amoba.common.player;

public class Player {
	private String name;
	private String address;

	public Player(String name, String address) {
		this.name = name;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
