package hu.bme.szarch.amoba.common.player;

public class PlayerStats {
	private int id;
	private int wins;
	private int losses;

	public PlayerStats(int id, int wins, int losses) {
		this.id = id;
		this.wins = wins;
		this.losses = losses;
	}

	public int getId() {
		return id;
	}

	public int getLosses() {
		return losses;
	}

	public int getWins() {
		return wins;
	}
}
