package hu.bme.szarch.amoba.common.game;

import hu.bme.szarch.amoba.common.player.Player;

public class BasicGameData implements GameData {

	private int id;
	private Player playerHost;
	private Player playerGuest;
	private int width;
	private int height;
	private int winCriteria;

	public BasicGameData(int id, int width, int height, int winCriteria, Player host, Player guest) {
		this.id = id;
		this.width = width;
		this.height = height;
		this.winCriteria = winCriteria;
		this.playerHost = host;
		this.playerGuest = guest;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public int getWinCriteria() {
		return winCriteria;
	}

	@Override
	public Player getHost() {
		return playerHost;
	}

	@Override
	public Player getGuest() {
		return playerGuest;
	}

	@Override
	public void setGuest(Player guest) {
		this.playerGuest = guest;
	}

}
