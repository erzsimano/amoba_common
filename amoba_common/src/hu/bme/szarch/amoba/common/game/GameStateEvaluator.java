package hu.bme.szarch.amoba.common.game;

import hu.bme.szarch.amoba.common.table.GameTableModel;

public class GameStateEvaluator {

	public static final int STATE_NOTHING = -1;
	public static final int STATE_DRAW = 0;
	public static final int STATE_O_WON = 1;
	public static final int STATE_X_WON = 2;

	public static int evaluateGameState(GameTableModel table, int winCriteria, int x, int y, short mark) {
		System.out.println("GameStateEvaluator.evaluateGameState not yet implemented");
		return STATE_NOTHING;
	}

}
