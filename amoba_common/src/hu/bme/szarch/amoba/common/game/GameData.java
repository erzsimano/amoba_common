package hu.bme.szarch.amoba.common.game;

import hu.bme.szarch.amoba.common.player.Player;

public interface GameData {
	public int getId();

	public int getWidth();

	public int getHeight();

	public int getWinCriteria();

	public Player getHost();

	public Player getGuest();

	public void setGuest(Player guest);

}
