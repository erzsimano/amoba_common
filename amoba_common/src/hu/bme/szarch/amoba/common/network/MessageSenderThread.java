package hu.bme.szarch.amoba.common.network;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class MessageSenderThread extends Thread {

	public interface MessageSenderListener {
		public void onMessageSenderError(String errorMsg);

		public void onMessageSent();
	}

	private NetworkMessage object;
	private String address;
	private int port;

	private List<MessageSenderListener> listeners;

	public MessageSenderThread(NetworkMessage object, String address, int port) {
		this.object = object;
		this.address = address;
		this.port = port;

		listeners = new ArrayList<>();
	}

	public void addListener(MessageSenderListener listener) {
		listeners.add(listener);
	}

	@Override
	public void run() {

		Socket socket = null;
		OutputStream os = null;
		ObjectOutputStream oos = null;
		try {

			socket = new Socket(InetAddress.getByName(address), port);
			os = socket.getOutputStream();
			oos = new ObjectOutputStream(os);
			oos.writeObject(object);

			for (MessageSenderListener listener : listeners) {
				listener.onMessageSent();
			}
		} catch (UnknownHostException e1) {
			System.out.println("Unknown host!" + address + ":" + port);
		} catch (IOException e2) {
			for (MessageSenderListener listener : listeners) {
				listener.onMessageSenderError("IOException in MessageSender! " + e2.getMessage());
			}
		} finally

		{
			try {
				if (oos != null)
					oos.close();
				if (os != null)
					os.close();
				if (socket != null)
					socket.close();
			} catch (Exception e2) {
				System.out.println("Something seriously went wrong...");
			}
		}
	}

}
