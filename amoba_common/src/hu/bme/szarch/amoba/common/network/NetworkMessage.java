package hu.bme.szarch.amoba.common.network;

import java.io.Serializable;

public interface NetworkMessage extends Serializable {

	public String getData(String key);

	public String getId();

	void setData(String key, String value);
}
