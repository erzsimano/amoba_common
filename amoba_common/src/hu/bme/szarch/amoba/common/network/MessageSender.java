package hu.bme.szarch.amoba.common.network;

public interface MessageSender {
	public void sendMessage(NetworkMessage message);
}
