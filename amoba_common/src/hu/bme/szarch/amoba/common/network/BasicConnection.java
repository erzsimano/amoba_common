package hu.bme.szarch.amoba.common.network;

import java.util.HashMap;
import java.util.Map;

import hu.bme.szarch.amoba.common.config.Config;
import hu.bme.szarch.amoba.common.network.factory.NetworkObjectFactory;

public class BasicConnection implements Connection {

	private String remoteAddress;
	private int remotePort;

	private MessageReceiverThread receiver;
	private MessageSender sender;

	private NetworkObjectFactory messageFactory;

	private Map<String, MessageHandler> messageHandlers;

	private boolean isConnected;

	public BasicConnection(String remoteAddress, int remotePort, int localPort) {
		this.remoteAddress = remoteAddress;
		this.remotePort = remotePort;

		this.receiver = new MessageReceiverThread(localPort);
		this.sender = new BasicMessageSender(remoteAddress, remotePort);

		this.messageFactory = NetworkObjectFactory.create();

		this.messageHandlers = new HashMap<>();

		this.isConnected = false;
	}

	@Override
	public void connect() {
		NetworkMessage message = messageFactory.createConnectionRequest();
		sender.sendMessage(message);
	}

	@Override
	public void disconnect() {

	}

	@Override
	public boolean isConnected() {
		return isConnected;
	}

	@Override
	public void sendMessage(NetworkMessage message) {
		sender.sendMessage(message);
	}

	@Override
	public void registerMessageHandler(MessageHandler handler) {
		if (handler != null)
			messageHandlers.put(handler.getHandleId(), handler);
	}

	@Override
	public void unregisterMessageHandler(MessageHandler handler) {
		if (handler != null)
			messageHandlers.remove(handler.getHandleId());
	}

	@Override
	public void routeMessage(NetworkMessage message) {

		switch (message.getId()) {
		case Config.ID_CONNECTION_OK:
			isConnected = true;
			break;
		default:
			MessageHandler handler = messageHandlers.get(message.getId());
			if (handler != null)
				handler.handleMessage(message);
			else
				System.out.println("No handler available for " + message.getId() + " messages!");
			break;
		}
	}
}
