package hu.bme.szarch.amoba.common.network.factory;

import java.util.List;

import hu.bme.szarch.amoba.common.game.GameData;
import hu.bme.szarch.amoba.common.network.NetworkMessage;

public abstract class NetworkObjectFactory {
	public abstract NetworkMessage createMarkDataObject(int x, int y, short mark);

	public abstract NetworkMessage createPlayerDataObject(String playerName);

	public abstract NetworkMessage createPlayerStatsObject(String playerName, int wins, int loses);

	public abstract NetworkMessage createConnectionRequest();

	public abstract NetworkMessage createLoginRequest(String userName, String passwd);

	public abstract NetworkMessage createConnectionResponse(String requestCode, String responseMessage);

	public static NetworkObjectFactory create() {
		return new MapNetworkObjectFactory();
	}

	public abstract NetworkMessage createHostedGameList(List<GameData> games);

	public abstract NetworkMessage createStartGameMessage(String name, int whichMark);

	public abstract NetworkMessage createEndGameMessage();
}
