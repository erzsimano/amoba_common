package hu.bme.szarch.amoba.common.network;

public interface MessageHandler {
	public boolean handleMessage(NetworkMessage message);

	public String getHandleId();
}
