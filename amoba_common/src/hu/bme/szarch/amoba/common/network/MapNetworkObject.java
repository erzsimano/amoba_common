package hu.bme.szarch.amoba.common.network;

import java.util.HashMap;
import java.util.Map;

public class MapNetworkObject implements NetworkMessage {

	private static final long serialVersionUID = -7032995061521379893L;
	private final String id;
	private final Map<String, String> data;

	public MapNetworkObject(String id) {
		this.id = id;
		this.data = new HashMap<>();
	}

	@Override
	public void setData(String key, String value) {
		data.put(key, value);
	}

	@Override
	public String getData(String key) {
		return data.get(key);
	}

	@Override
	public String getId() {
		return id;
	}

}
