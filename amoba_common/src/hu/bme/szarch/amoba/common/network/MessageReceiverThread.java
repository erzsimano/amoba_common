package hu.bme.szarch.amoba.common.network;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import hu.bme.szarch.amoba.common.config.Config;

public class MessageReceiverThread extends Thread {

	private volatile boolean isRunning = true;

	private int listenPort;

	private List<NetworkMessageRouter> messageRouters;

	public MessageReceiverThread(int listenPort) {
		messageRouters = new ArrayList<>();
		this.listenPort = listenPort;
	}

	public void addMessageRouter(NetworkMessageRouter listener) {
		messageRouters.add(listener);
	}

	public void removeMessageRouter(NetworkMessageRouter listener) {
		messageRouters.remove(listener);
	}

	public void stopListening() {
		isRunning = false;
	}

	@Override
	public void run() {
		while (isRunning) {

			ServerSocket serverSocket = null;
			Socket socket = null;
			InputStream is = null;
			ObjectInputStream ois = null;

			try {
				serverSocket = new ServerSocket(listenPort);
				serverSocket.setSoTimeout(Config.TIMEOUT_MILLIS);
				System.out.println("Listening for incoming objects...");
				socket = serverSocket.accept();
				is = socket.getInputStream();
				ois = new ObjectInputStream(is);
				NetworkMessage object = (NetworkMessage) ois.readObject();
				SocketAddress senderAddress = socket.getRemoteSocketAddress();
				if (object != null) {
					String address = senderAddress.toString().replaceAll("/|(:.*)", "");
					System.out.println("Object received: " + object.getId() + " from: " + address);
					object.setData(Config.DATA_SENDER_ADDRESS, address);
					for (NetworkMessageRouter router : messageRouters) {
						router.routeMessage(object);
					}
				}
				if (is != null)
					is.close();
				if (socket != null)
					socket.close();
				if (serverSocket != null)
					serverSocket.close();

			} catch (SocketTimeoutException te) {
				try {
					// te.printStackTrace();
					System.out.println("SocketTimeout occured, listening again...");
					if (is != null)
						is.close();
					if (socket != null)
						socket.close();
					if (serverSocket != null)
						serverSocket.close();
				} catch (Exception e2) {
					System.out.println("Something seriously went wrong...");
				}
			} catch (Exception e) {
				try {
					e.printStackTrace();
					System.out.println("Exception:" + e.getMessage());
					if (is != null)
						is.close();
					if (socket != null)
						socket.close();
					if (serverSocket != null)
						serverSocket.close();
				} catch (Exception e2) {
					System.out.println("Something seriously went wrong...");
				}
			}
		}
		System.out.println("Object listener shutting down.");
	}
}
