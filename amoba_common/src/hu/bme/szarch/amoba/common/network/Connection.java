package hu.bme.szarch.amoba.common.network;

public interface Connection extends NetworkMessageRouter {

	public void connect();

	public void disconnect();

	public boolean isConnected();

	public void sendMessage(NetworkMessage message);

}
