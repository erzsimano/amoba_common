package hu.bme.szarch.amoba.common.network;

public class BasicMessageSender implements MessageSender {

	private String address;
	private int port;

	public BasicMessageSender(String address, int port) {
		this.address = address;
		this.port = port;
	}

	@Override
	public void sendMessage(NetworkMessage message) {
		System.out.println("Sending message with ID: " + message.getId() + " to " + address + ":" + port);
		new MessageSenderThread(message, address, port).start();
	}
}
