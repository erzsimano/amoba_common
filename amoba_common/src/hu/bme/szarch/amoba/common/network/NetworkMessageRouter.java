package hu.bme.szarch.amoba.common.network;

public interface NetworkMessageRouter {
	public void registerMessageHandler(MessageHandler handler);

	public void unregisterMessageHandler(MessageHandler handler);

	public void routeMessage(NetworkMessage message);
}
