package hu.bme.szarch.amoba.common.network.factory;

import java.util.List;

import static hu.bme.szarch.amoba.common.config.Config.DATA_CONNECTION_RESPONSE_MESSAGE;
import static hu.bme.szarch.amoba.common.config.Config.ID_CONNECTION_REQUEST;
import static hu.bme.szarch.amoba.common.config.Config.ID_CONNECTION_RESPONSE;
import static hu.bme.szarch.amoba.common.config.Config.ID_END_GAME;
import static hu.bme.szarch.amoba.common.config.Config.ID_LIST_GAMES;
import static hu.bme.szarch.amoba.common.config.Config.ID_LOGIN_REQUEST;
import static hu.bme.szarch.amoba.common.config.Config.ID_MARK_DATA;
import static hu.bme.szarch.amoba.common.config.Config.ID_PLAYER_DATA;
import static hu.bme.szarch.amoba.common.config.Config.ID_PLAYER_STATS_DATA;
import static hu.bme.szarch.amoba.common.config.Config.ID_START_GAME;

import hu.bme.szarch.amoba.common.config.Config.ListGameFields;
import hu.bme.szarch.amoba.common.config.Config.LoginFields;
import hu.bme.szarch.amoba.common.config.Config.MarkFields;
import hu.bme.szarch.amoba.common.config.Config.PlayerFields;
import hu.bme.szarch.amoba.common.game.GameData;
import hu.bme.szarch.amoba.common.network.MapNetworkObject;
import hu.bme.szarch.amoba.common.network.NetworkMessage;

public class MapNetworkObjectFactory extends NetworkObjectFactory {
	@Override
	public NetworkMessage createMarkDataObject(int x, int y, short mark) {
		NetworkMessage message = new MapNetworkObject(ID_MARK_DATA);
		message.setData(MarkFields.DATA_MARK_COORD_X, Integer.toString(x));
		message.setData(MarkFields.DATA_MARK_COORD_Y, Integer.toString(y));
		message.setData(MarkFields.DATA_MARK, Short.toString(mark));
		return message;
	}

	@Override
	public NetworkMessage createPlayerDataObject(String playerName) {
		NetworkMessage message = new MapNetworkObject(ID_PLAYER_DATA);
		message.setData(PlayerFields.DATA_PLAYER_NAME, playerName);
		return message;
	}

	public NetworkMessage createPlayerStatsObject(String playerName, int wins, int loses) {
		NetworkMessage message = new MapNetworkObject(ID_PLAYER_STATS_DATA);
		message.setData(PlayerFields.DATA_PLAYER_NAME, playerName);
		// message.setData(DATA_PLAYER_WINS, Integer.toString(wins));
		// message.setData(DATA_PLAYER_LOSES, Integer.toString(loses));
		return message;
	}

	@Override
	public NetworkMessage createConnectionRequest() {
		NetworkMessage message = new MapNetworkObject(ID_CONNECTION_REQUEST);
		return message;
	}

	@Override
	public NetworkMessage createConnectionResponse(String requestCode, String responseMessage) {
		NetworkMessage message = new MapNetworkObject(ID_CONNECTION_RESPONSE);
		// message.setData(DATA_REQUEST_CODE, requestCode);
		message.setData(DATA_CONNECTION_RESPONSE_MESSAGE, responseMessage);
		return message;
	}

	@Override
	public NetworkMessage createHostedGameList(List<GameData> games) {
		NetworkMessage message = new MapNetworkObject(ID_LIST_GAMES);
		StringBuffer buffer = new StringBuffer();
		for (GameData game : games) {
			buffer.append(game.getId()).append(";");
			buffer.append(game.getWidth()).append(";");
			buffer.append(game.getHeight()).append(";");
			buffer.append(game.getWinCriteria()).append("||");
		}
		buffer.substring(0, buffer.length() - 2);
		message.setData(ListGameFields.DATA_GAME_LIST, buffer.toString());
		return message;
	}

	@Override
	public NetworkMessage createStartGameMessage(String opponentName, int whichMark) {
		NetworkMessage message = new MapNetworkObject(ID_START_GAME);

		message.setData(PlayerFields.DATA_PLAYER_NAME, opponentName);
		message.setData(MarkFields.DATA_MARK, Integer.toString(whichMark));
		return message;
	}

	@Override
	public NetworkMessage createEndGameMessage() {
		NetworkMessage message = new MapNetworkObject(ID_END_GAME);

		return message;
	}

	@Override
	public NetworkMessage createLoginRequest(String userName, String passwd) {
		NetworkMessage message = new MapNetworkObject(ID_LOGIN_REQUEST);
		message.setData(LoginFields.DATA_USERNAME, userName);
		message.setData(LoginFields.DATA_PASSWORD, passwd);
		return message;
	}
}
