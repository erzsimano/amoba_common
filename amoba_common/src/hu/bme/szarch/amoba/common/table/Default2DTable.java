
package hu.bme.szarch.amoba.common.table;

import java.util.ArrayList;
import java.util.List;

import hu.bme.szarch.amoba.common.exception.InvalidTableFieldException;

public class Default2DTable implements GameTableModel {

	private int columnCount = 0;
	private int rowCount = 0;
	private List<Cell> cells;

	/**
	 * Creates a Pattern with the given attributes.
	 * 
	 * @param colCount
	 *            Number of Columns
	 * @param rowCount
	 *            Number of Rows
	 * @param cells
	 *            An existing list of cells, if null, an empty pattern will be
	 *            created
	 */
	public Default2DTable(int colCount, int rowCount, List<Cell> cells) {
		this.columnCount = colCount;
		this.rowCount = rowCount;
		this.cells = cells;

		if (this.cells == null && cells != null) {
			this.cells = new ArrayList<Cell>(cells.size());
			this.cells.addAll(cells);
		}
	}

	/**
	 * Creates a Pattern containing <b>colCount</b> columns and <b>rowCount</b>
	 * rows, all cells initialized empty.
	 * 
	 * @param colCount
	 *            Number of columns
	 * @param rowCount
	 *            Number of rows
	 */
	public Default2DTable(int colCount, int rowCount) {
		this.columnCount = colCount;
		this.rowCount = rowCount;
		int cellCount = colCount * rowCount;

		this.cells = new ArrayList<Cell>(cellCount);
		for (int idx = 0; idx < cellCount; idx++) {
			cells.add(new Cell());
		}
	}

	@Override
	public void setMark(int x, int y, short mark) throws InvalidTableFieldException {
		int idx = getArrayIndex(x, y);
		if (cells.size() <= idx)
			throw new InvalidTableFieldException();
		cells.get(idx).setMark(mark);
	}

	@Override
	public short getMarkAt(int x, int y) throws InvalidTableFieldException {
		int idx = getArrayIndex(x, y);
		if (cells.size() <= idx)
			throw new InvalidTableFieldException();
		return cells.get(idx).getMark();
	}

	public int getColumnCount() {
		return columnCount;
	}

	public int getRowCount() {
		return rowCount;
	}

	private int getArrayIndex(int x, int y) {
		return y * columnCount + x;
	}
}
