package hu.bme.szarch.amoba.common.table;

import hu.bme.szarch.amoba.common.exception.InvalidTableFieldException;

public interface GameTableModel {

	public void setMark(int x, int y, short mark) throws InvalidTableFieldException;

	public short getMarkAt(int x, int y) throws InvalidTableFieldException;

	public int getColumnCount();

	public int getRowCount();
}
