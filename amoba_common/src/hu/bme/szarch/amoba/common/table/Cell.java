
package hu.bme.szarch.amoba.common.table;

import java.io.Serializable;

import hu.bme.szarch.amoba.common.exception.InvalidMarkException;

public class Cell implements Serializable {

	// Valid cell values
	public static final short CELL_VALUE_EMPTY = -1;
	public static final short CELL_VALUE_O = 0;
	public static final short CELL_VALUE_X = 1;

	private short mark;

	public Cell() {
		mark = CELL_VALUE_EMPTY;
	}

	public short getMark() {
		return mark;
	}

	public void setMark(short mark) {
		if (mark != CELL_VALUE_EMPTY || mark != CELL_VALUE_O || mark != CELL_VALUE_X)
			throw new InvalidMarkException();
		this.mark = mark;
	}
}
